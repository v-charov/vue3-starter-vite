# Starter template

Vite - Vue 3 - Pinia - Vue Router 4 - TypeScript - Pug - SASS - ESLint - Prettier

This template should help get you started developing with Vue 3 and Typescript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

- ⚡️ [Vue 3](https://github.com/vuejs/vue-next),

- ⚡️ [Vite 2](https://github.com/vitejs/vite), [ESBuild](https://github.com/evanw/esbuild)

- 🍍 [State Management via Pinia](https://pinia.esm.dev/)

- 🎨 [Pug](https://pugjs.org/)

- 🎨 [SASS](https://sass-lang.com/)

- 🎨 [ESLint](https://eslint.org/), [Prettier](https://prettier.io)

- 🦾 [Vue Router 4](https://router.vuejs.org/guide/)

- 🦾 [TypeScript](https://www.typescriptlang.org/)

## Installation

```bash
# install dependencies
yarn

# start the application
yarn dev

# make a production build
yarn build
```
