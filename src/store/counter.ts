import { defineStore } from "pinia";

export const useStore = defineStore("storeCounter", {
  state: () => ({
    counter: 0,
  }),
});
